#!/bin/bash

# help text
function usage(){
    echo "usage: ./lint-all.sh HOST PORT"
}

function lint(){
    echo "Linting $3"
    JENKINS_URL="$1:$2"
    JENKINS_CRUMB=`curl "$JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)"`
    RESPONSE=`curl -X POST -H $JENKINS_CRUMB -F "jenkinsfile=<$3" $JENKINS_URL/pipeline-model-converter/validate`
    if [ "$RESPONSE" == "Jenkinsfile successfully validated." ]
    then 
        echo "$file successfully validated"
    else 
        echo "RESPONSE: $RESPONSE"
        echo "$file contains linting errors."
        exit 1
    fi
}

HOST=$1
PORT=$2

if [ -z "${HOST}" ] || [ -z "${PORT}" ]
then
  usage
  return
fi

for file in ./vars/*ipeline*.groovy; do
    echo "Linting $file"
    lint $HOST $PORT $file
done