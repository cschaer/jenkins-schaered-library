package com.apiture.jenkins

/**
 * Checkouts source code from specified branch
 */
def checkout(def repository, def branch, def dir = ".") {
    git branch: "${branch}",
        url: "${repository}",
        credentialsId: "apitureqa"
//     checkout([$class: "GitSCM",
//         branches: [[name: "${branch}"]],
//         doGenerateSubmoduleConfigurations: false,
//         extensions: [[$class: "RelativeTargetDirectory", relativeTargetDir: "${dir}"]],
//         submoduleCfg: [], 
//         userRemoteConfigs: [[credentialsId: "apitureqa", url: "${repository}"]]])
}

/**
 * Returns the current build's branch
 */
def getBranch() {
    return env.BRANCH_NAME
}

/**
 * Returns the current build's target
 */
def getTarget() {
    return env.CHANGE_TARGET
}

/**
 * Get Git commit SHA
 */
def getGitSHA() {
    return sh (script: "git rev-parse ${env.GIT_COMMIT}", returnStdout: true)
}

/**
 * Get Git commit SHA (short)
 */
def getGitSHAShort() {
    return sh (script: "git rev-parse --short=7 ${env.GIT_COMMIT}", returnStdout: true)
}

/**
 * Get Git commit author (committer)
 */
def getGitCommitter() {
    def committer = sh (script: "git log --format='%ce' ${env.GIT_COMMIT}^!", returnStdout: true)
    def author    = sh (script: "git log --format='%ae' ${env.GIT_COMMIT}^!", returnStdout: true)

    return isPullRequest() ? committer : author
}