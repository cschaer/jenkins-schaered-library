def call(body) {
    // asks for authorization, then calls build(release-tool)

    pipeline {
        agent any

        stages {
            stage('Authorize') {
                // logic is needed for whether team1 oder team2 is allowed to ok here
                input {
                    id "Authorization"
                    message "Are you Authorized to run this build?"
                    ok "Yes"
                }
                steps {
                    build (
                        job: "release-tool",
                        propagate: true
                    )
                }
            }
        }
    }
}