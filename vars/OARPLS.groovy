def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def timeoutInMinutes = config.timeout ?: 30

    pipeline {
        agent any

        // parameter for linting?

        stages {
            stage('Eins') {
                //input {
                //    id "Input 1"
                //    message "Wilkommen in mein Testprogramm"
                //    ok "ja ja ja"
                //}
                steps {
                    echo "$PWD"
                    echo "stage one is a go"
                }
            }
            stage('Zwei') {
                steps {
                    build(
                        job: "lint-roller",
                        propagate: true
                    )
                }
            }
        }
    }
}