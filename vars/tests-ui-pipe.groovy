def call(body){
    pipeline {
        agent { label "build1" }

        parameters {
            choice(name: "BROWSER", choices:["chrome", "firefox"], description: "The browser that tests will execute in. If the browser isn't listed, it's not currently supported.")
            choice(name: "EXECUTION", choices:["sequential"], description: "The execution method is used to run the tests. Sequential is slowest but most stable.")
            string(name: "BRANCH", defaultValue:"master")
            string(name: "RERUNCOUNT", defaultValue:"1")
            // extended choice parameters
            choice(name: "ENVIRONMENT", choices:["QA-Integration", "PROD", "UAT", "UAT2", "QA1"], description: "The environment on which to run the tests. (e.g. UAT)")
            choice(name: "CATEGORY", choices:["Smoke Tests", "All Tests"], description: "The Category of tests to run. (e.g. SmokeTest)")

        }

        options {
            buildDiscarder(logRotator(numToKeepStr: '20'))
        }

        triggers {
            // upstream project is inheritor of griffin-api-tests
            upstream(upstreamProjects: 'community-tests-api', threshold: hudson.model.Result.SUCCESS)
            pollSCM('H/15 * * * *')
        }

        stages {
            stage('Resolve Parameters') {
                steps {
                    script {
                        switch(params.ENVIRONMENT) { // switch for ENVIRONMENT parameter choice
                            case "UAT":
                                params.ENVIRONMENT = "https://uat.fundsxpress.com"
                                break
                            case "UAT2":
                                params.ENVIRONMENT = "https://fxws-cat.fundsxpress.com"
                                break
                            case "QA1":
                                params.ENVIRONMENT = "https://qa1-griffin.aws.apiture.com:8443"
                                break
                            case "PROD":
                                params.ENVIRONMENT = "https://secure.fundsxpress.com"
                                break
                            case "QA-Integration":
                                params.ENVIRONMENT = "https://qa-int-fxweb.apiture-comm-nonprod.com"
                                break
                        }

                        switch(params.CATEGORY) { // switch for CATEGORY parameter choice
                            case "All Tests":
                                params.CATEGORY = ""
                                break
                            case "Smoke Tests":
                                params.CATEGORY = "com.apitureqa.core.categories.SmokeTest"
                                break
                        }
                    }
                }
            }

            stage('Source'){
                steps {
                    Git.checkout('https://apitureqa@bitbucket.org/APITURE/community-ui-automation.git', "master")
                }
            }

            stage('Build'){
                agent {
                    docker {
                            image "maven:alpine"
                            args "-u root"
                            reuseNode true
                    }
                }
                steps {
                    // maven settings?
                    sh "mvn clean install test -Dbrowser=${params.BROWSER} -Drun_locally=false -Dgroups=${params.CATEGORY} -Denvironment=${params.ENVIRONMENT}"
                }
                post {
                    success {
                        //slack
                        sh "echo 'success'"
                    }
                }
            }
        }
    }
}
