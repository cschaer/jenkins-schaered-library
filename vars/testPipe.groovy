import java.util.Random
@Library('jenkins-schaered-library') _

def call(body) {
    
    def utilities = new com.apiture.Utilities()

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body() 

    def skipTests = config.skipTests ?: false
    def timeoutInMinutes = config.timeout ?: 45
    def templateTest = config.templateTest ?: false
    def numberOfBuildsToKeep = utilities.getNumberOfBuildsToKeep()
    def numberOfDaysToKeepBuild = utilities.getNumberOfDaysToKeepBuild()
    def agentLabel = utilities.getAgentLabel()

    env.templateTest = templateTest
    env.PORTAL_BUCKET_KEY = "SchemaWebPortalBucketName"
    env.SOURCE_FILES_DIRECTORY = "dist"

    pipeline {
        agent {
            label "${agentLabel}"
        }

        parameters {
            string(name: "RELEASE_MODE", defaultValue: '', description: '')
            string(name: 'AWS_ENVIRONMENT', defaultValue: '', description: 'The name of the target AWS environment.')
            string(name: 'AWS_ENV_ACCOUNT_ID', defaultValue: '', description: 'The ID of the target AWS environment.')
            string(name: 'AWS_ENV_ROLE', defaultValue: '', description: 'The role to assume in the target AWS environment.')
            string(name: 'AWS_ENV_PROFILE', defaultValue: '', description: 'The AWS credentials profile with access to the target AWS environment.')
            string(name: 'AWS_ENV_STAGE', defaultValue: '', description: 'The stage of the target AWS environment.')
            string(name: 'AWS_REGION', defaultValue: 'us-east-1', description: 'The region to deploy resources into.')
            string(name: 'RELEASE_BRANCH', defaultValue: '', description: 'The branch to deploy to the target AWS environment.')
        }

        options {
            timeout(time: timeoutInMinutes, unit: 'MINUTES')
            buildDiscarder(
                logRotator(
                        numToKeepStr: "${numberOfBuildsToKeep}",
                        daysToKeepStr: "${numberOfDaysToKeepBuild}",
                        artifactNumToKeepStr: "${numberOfBuildsToKeep}",
                        artifactDaysToKeepStr: "${numberOfDaysToKeepBuild}"
                )
            )
            disableConcurrentBuilds()
        }

        stages {
            stage('Initialize') {
                steps {
                    initializeEnvVariables(params)
                    initializePrEnvVariables(params)

                    echo 'Modifying package.json config variables'
                    script {
                        def packageJSON = readJSON file: 'package.json'
                        packageJSON.config.awsNetworkStack = "network-stack-${env.AWS_ENVIRONMENT}".toString()
                        packageJSON.config.awsEnv = "${env.AWS_ENVIRONMENT}".toString()
                        packageJSON.config.awsAccount = "${env.AWS_ENV_ACCOUNT_ID}".toString()
                        packageJSON.config.awsRole = "${env.AWS_ENV_ROLE}".toString()
                        packageJSON.config.awsProfile = "${env.AWS_ENV_PROFILE}".toString()
                        packageJSON.config.awsRegion = "${env.AWS_ENV_REGION}".toString()

                        if(utilities.isPullRequest()){
                            packageJSON.config.awsStageName = "${utilities.getPullRequestNumber()}".toString()

                            withAWS(role:env.AWS_ENV_ROLE, roleAccount:env.AWS_ENV_ACCOUNT_ID, profile:env.AWS_ENV_PROFILE, region:env.AWS_ENV_REGION) {
                                def nlbArn = utilities.getCloudFormationExportValue('ecs-stack-NetworkLoadBalancer')
                                def portsInUse = sh(
                                    script: "aws elbv2 describe-listeners --load-balancer-arn ${nlbArn} --query 'Listeners[].Port'",
                                    returnStdout: true
                                )

                                int minPort = "${packageJSON.config.defaultProdPort}".toInteger() + 3
                                int portRange = 97
                                Boolean isUnused = false
                                Random rand = new Random() 

                                while (!isUnused){
                                    def port = rand.nextInt(portRange) + minPort
                                    echo "${port}"
                                    if (!portsInUse.contains(port.toString())){
                                        echo "Port ${port} is not in use."
                                        packageJSON.config.defaultPullRequestPort = "${port}".toInteger()
                                        utilities.setPullRequestPort(port)
                                        isUnused = true
                                    } else {
                                        echo "Port ${port} is already in use.  Generating another rando port."
                                    }
                                }
                            }
                        } else {
                            packageJSON.config.awsStageName = "prod"
                        }

                        writeJSON file: 'package.json', json: packageJSON, pretty: 4
                    }
                }
            }
            stage('ECR Version Check'){
                when {
                    expression { utilities.isPullRequest() }
                }
                steps {
                    runEcrVersionCheck {}
                }
            }
            stage('Install') {
                steps {
                    npmInstall {}
                }
            }
            stage('Generate') {
                steps {
                    npmGenerate {}
                }
            }
            stage('Test') {
                when {
                    expression { !utilities.isReleaseMode(params) }
                }
                parallel {
                    stage('Unit Test') {
                        steps {
                            runUnitTests {}
                        }
                    }
                    stage('Lint') {
                        steps {
                            runTypeScriptLint {}
                        }
                    }
                }

            }
            stage('Analyze') {
                when {
                    expression { !utilities.isReleaseMode(params) }
                }
                parallel {
                    stage('Code Coverage') {
                        steps {
                            runCodeCoverage {}
                        }
                    }
                }
            }
            stage('Tsc') {
                steps {
                    sh "npm run dockerBuild"
                }
            }
            stage('Build Image') {
                steps {
                    sh "npm run dockerBuildImage"
                }
            }
            stage('Deploy Image') {
                steps {
                    sh "npm run dockerDeployImage"
                }
            }
            stage ('Deploy API') {
                parallel {
                    stage('Deploy Service') {
                        steps {
                            sh "npm run dockerDeployService"
                        }
                    }
                    stage('Deploy API') {
                        steps {
                            echo "Pre dockerDeployApi"
                            sh "npm run dockerDeployApi"

                            echo "Post dockerDeployApi"
                            // script {
                            //     utilities.deploySchemaToS3Bucket("${env.AWS_ENV_ACCOUNT_ID}","${env.AWS_ENV_ROLE}","${env.AWS_ENVIRONMENT}","${env.BUILD_RUNNER_PROFILE}", "${env.PORTAL_BUCKET_KEY}", "${env.SOURCE_FILES_DIRECTORY}")
                            // }
                            script {
                                echo "In script block"
                                env.API_URL = ''

                                withAWS(role:env.AWS_ENV_ROLE, roleAccount:env.AWS_ENV_ACCOUNT_ID, profile:env.AWS_ENV_PROFILE, region:env.AWS_ENV_REGION) {
                                    echo "In withAWS block"
                                    if (utilities.isPullRequest()){
                                        //TODO: better handling of repo names that get truncated due to AWS length limits
                                        def service = utilities.getApi() == "biz-verifications" ? "business-verifications" : utilities.getApi()
                                        env.API_URL = utilities.getCloudFormationExportValue("${service}-api-${utilities.getPullRequestNumber()}-PullRequestEndpointURL")
                                        assert "${utilities.getApiUrl()}" != "" : "${service}-api-${utilities.getPullRequestNumber()}-PullRequestEndpointURL returned an empty string"
                                        env.SERVICE_HOST = utilities.getApiUrl()
                                    } else {
                                        env.SERVICE_HOST = "https://${utilities.getCloudFormationExportValue('ApiGatewayCustomDomainName')}"
                                    }

                                    echo "env.API_URL = ${utilities.getApiUrl()}"
                                    echo "env.SERVICE_HOST = ${env.SERVICE_HOST}"
                                }
                            }
                        }
                    }
                }
                post {
                    success {
                        script {
                            if (!utilities.isPullRequest()){
                                sh "npm run dockerSwapApiStages"
                                utilities.deploySchemaToS3Bucket("${env.AWS_ENV_ACCOUNT_ID}","${env.AWS_ENV_ROLE}","${env.AWS_ENVIRONMENT}","${env.BUILD_RUNNER_PROFILE}", "${env.PORTAL_BUCKET_KEY}", "${env.SOURCE_FILES_DIRECTORY}")
                            }
                            //sleeping while dns propogates. maybe. who knows
                            sleep(30)
                        }
                    }
                }
            }
            stage('Integration Test') {
                when {
                    expression { utilities.shouldRunIntegrationTests(skipTests) && !utilities.isReleaseMode(params) }
                }
                parallel {
                    stage('API') {
                        steps {
                            // echo "Pretend the tests passed"
                            runApiIntegrationTests {}
                        }
                        post {
                            always {
                                archiveTestResults {}
                                // echo "always"
                            }
                        }
                    }
                    stage('UI') {
                        when {
                            expression { !utilities.isReleaseMode(params) && utilities.shouldRunUiTests() }//remove this once ui tests exist
                        }
                        steps {
                            runUiIntegrationTests {}
                        }
                    }
                }
            }
        }
        post {
            always {
                sendNotifications(
                        currentBuild.result,
                        config.buildChannel ?: (utilities.isReleaseMode(params) ? '#open-release-results' : '#open-cicd'),
                        config.pullRequestChannel ?: (utilities.isReleaseMode(params) ? '#open-release-results' : '#open-pull-requests')
                )
                archiveBuildArtifacts {}
                
                sh "npm run dockerStopStagingTasks"
                sh "npm run dockerRemoveStagingService"
            }
            success {
                undeployTempStack {}
                cleanWs()
            }
        }
    }
}
