import com.apiture.docker.Docker

def call(String image) {
    def docker = new Docker()
    docker.build(image)
}