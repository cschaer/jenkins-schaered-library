
// THIS FILE IS USELESS AND CAN BE REMOVED



def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    pipeline {
        // parameter of jenkinsfile to test 
        agent any

        stages {
            stage ('Lint') {
                steps {
                    // run validation test on each desired pipeline in the pulled repo
                    // the files to be checked could theoretically be parameterized
                    // this works by sshing into itself and running the cli
                    // if a validation fails, the pipeline exits with code 1
                    sh """
                    chmod 777 /var/jenkins_home/workspace/lint-roller@libs/jenkins-schaered-library/jsl-linter.sh
                    /var/jenkins_home/workspace/lint-roller@libs/jenkins-schaered-library/jsl-linter.sh
                    """
                }
            }
        }
    }
}