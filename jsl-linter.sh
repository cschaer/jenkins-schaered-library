#!/bin/bash
### Jenkins library linting ###

FAILS=0

echo "Linting files..."

###rm -rf jenkins-shared-library

# pull library down into local workspace to ensure the latest master file is being tested
###git clone git@bitbucket.org:APITURE/jenkins-shared-library.git > /dev/null 2>&1

##if [ -e jenkins-schaered-library ] 
##then tput setaf 3; echo "JSL cloned successfully"; tput sgr0
##else exit 1 
##fi

##for filename in /var/jenkins_home/workspace/lint-roller@libs/jenkins-schaered-library/vars/*Pipeline*.groovy; do
##    if java -jar /var/jenkins_home/war/WEB-INF/jenkins-cli.jar -noCertificateCheck -s http://localhost:8080/ declarative-linter<$filename | grep -q 'Jenkinsfile successfully validated' 
##    then tput setaf 2; echo "-------$(basename $filename)  successfully validated-------"; tput sgr0
##    else tput setaf 1; echo "-------$(basename $filename)  validation failed-------"; tput sgr0; let "FAILS++"
##    fi 
##done
###rm -rf jenkins-shared-library

for filename in /Users/cameron.schaer/source-control/jenkins-shared-library/vars/*Pipeline*.groovy; do
    if java -jar ../jenkins-cli.jar -noCertificateCheck -s http://localhost:8080/ declarative-linter<$filename >&2 | grep -q 'Jenkinsfile successfully validated' 
    then tput setaf 2; echo "-------$(basename $filename)  successfully validated-------"; tput sgr0
    else tput setaf 1; echo "-------$(basename $filename)  validation failed-------"; tput sgr0; let "FAILS++"
    fi 
done

if [ $FAILS -gt 0 ]
then
    echo "$FAILS failed validations"
    exit 1
else
    echo "All files validated."
fi